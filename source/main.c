/*---------------------------------------------------------------------------------
	NDS template
---------------------------------------------------------------------------------*/

#include <nds.h>
#include <maxmod9.h>
#include <stdio.h>

#include "horse.h"
#include "horse2.h"

#include "soundbank.h"
#include "soundbank_bin.h"

char *border =
	"------------------------------"
	"|                            |"
	"|                            |"
	"|                            |"
	"|                            |"
	"|                            |"
	"|                            |"
	"|                            |"
	"|                            |"
	"|                            |"
	"------------------------------";

int main() {

	/*----------------------------------
	    VIDEO STUFF begins here
	----------------------------------*/

	PrintConsole mainConsole, subConsole;

	//vramSetMainBanks(VRAM_A_MAIN_BG_0x06000000, VRAM_B_LCD, VRAM_C_SUB_BG_0x06200000, VRAM_D_LCD);	
	vramSetBankA(VRAM_A_MAIN_BG_0x06000000);
	vramSetBankB(VRAM_B_LCD);	
	vramSetBankC(VRAM_C_SUB_BG_0x06200000);	
	vramSetBankD(VRAM_D_LCD);	
	videoSetMode(MODE_3_2D | DISPLAY_BG3_ACTIVE);
	videoSetModeSub(MODE_3_2D | DISPLAY_BG3_ACTIVE);

	// bitmap backgrounds
	int mainBg3 = bgInit(3, BgType_Bmp8, BgSize_B8_256x256, 1, 0);
	int subBg3 = bgInitSub(3, BgType_Bmp8, BgSize_B8_256x256, 1, 0);
	bgScroll(mainBg3, 0, 32); // 256x256 image with 32 unused rows at the top
	bgScroll(subBg3, 0, 32);

	// consoles
	consoleInit(&mainConsole, 0, BgType_Text4bpp, BgSize_T_256x256, 2, 0, true, true);
	consoleInit(&subConsole, 0, BgType_Text4bpp, BgSize_T_256x256, 2, 0, false, true);	
	consoleSetWindow(&mainConsole, 1,1,30,22);
	consoleSetWindow(&subConsole, 1,1,30,22);

	consoleSelect(&mainConsole);
	iprintf(border);	
	iprintf("\x1b[3;0H\twoudl you belive me\n");
	iprintf("\tiffi  ttiodl you\n");
	iprintf("\tim ight start progmarming\n");
	iprintf("\tfor the ds , q.m.\n");
	
	consoleSelect(&subConsole);
	iprintf("\x1b[14;0H\n");
	iprintf(border);

	/*----------------------------------
	    AUDIO STUFF begins here
	----------------------------------*/

	mmInitDefaultMem((mm_addr)soundbank_bin);
	
	// load the module
	mmLoad( MOD_BGM );

	// load sound effects
	mmLoadEffect( SFX_BASS );

	// Start playing module
	mmStart( MOD_BGM, MM_PLAY_LOOP );

	mm_sound_effect bass = {
		{ SFX_BASS } ,			// id
		(int)(1.0f * (1<<10)),	// rate
		0,		// handle
		255,	// volume
		128,	// panning
	};

	while(1) {

		// draw backgrounds depending on touch position
		touchPosition touchPos;
		touchRead(&touchPos);
		iprintf("\x1b[14;0H\tTouch x = %04i, %04i\n", touchPos.rawx, touchPos.px);
		iprintf("\tTouch y = %04i, %04i\n", touchPos.rawy, touchPos.py);

		if (touchPos.rawx < 2000) {
			dmaCopy(horsePal, BG_PALETTE_SUB, 256*2);
			dmaCopy(horseBitmap, bgGetGfxPtr(subBg3), 256*256);			
			dmaCopy(horse2Pal, BG_PALETTE, 256*2);
			dmaCopy(horse2Bitmap, bgGetGfxPtr(mainBg3), 256*256);
		} else {
			dmaCopy(horsePal, BG_PALETTE, 256*2);
			dmaCopy(horseBitmap, bgGetGfxPtr(mainBg3), 256*256);			
			dmaCopy(horse2Pal, BG_PALETTE_SUB, 256*2);
			dmaCopy(horse2Bitmap, bgGetGfxPtr(subBg3), 256*256);
		}

		// play sound on keypress
		scanKeys();
		if ( keysDown() & KEY_A ) {
			mmEffectEx(&bass);
		}

		swiWaitForVBlank();
	}

	return 0;
}
